module.exports = {
  testEnvironment: 'node',
  testMatch: ['**/*.test.ts'],
  testPathIgnorePatterns: ['cdk*'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  collectCoverageFrom: ['**/*.ts'],
  coverageReporters: ['text', 'html', 'cobertura'],
  reporters: [
    'default',
    [
      'jest-junit',
      {
        suiteName: 'NEXT',
        classNameTemplate: '{classname}',
      },
    ],
  ],
};
