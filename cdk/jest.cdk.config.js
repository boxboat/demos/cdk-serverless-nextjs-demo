module.exports = {
	testEnvironment: 'node',
	testMatch: ['**/*.test.ts'],
	transform: {
		'^.+\\.tsx?$': 'ts-jest'
	},
	globalSetup: "./jest.setup.ts",
	collectCoverageFrom: [
		"**/*.ts"
	],
	coverageReporters: [
		"text",
		"html",
		"cobertura"
	],
	reporters: [
		"default",
		[
			"jest-junit",
			{
				suiteName: "CDK",
				classNameTemplate: "{classname}"
			}
		]
	]
}
