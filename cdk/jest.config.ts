import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
  testEnvironment: 'node',
  testMatch: ['**/*.test.ts'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  globalSetup: './jest.setup.ts',
  reporters: [
    'default',
    [
      'jest-junit',
      {
        suiteName: 'CDK',
        classNameTemplate: '{classname}',
      },
    ],
  ],
};

export default config;
