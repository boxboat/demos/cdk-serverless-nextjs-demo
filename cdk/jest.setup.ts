import { builder } from "./bin/cdk";

module.exports = async (): Promise<void> => {
	await builder.build();
};
