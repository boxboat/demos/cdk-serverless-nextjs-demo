import { Certificate, CertificateValidation } from "aws-cdk-lib/aws-certificatemanager";
import { CfnOutput, Stack } from "aws-cdk-lib";
import type { Construct } from "constructs";
import { HostedZone } from "aws-cdk-lib/aws-route53";
import { NextJSLambdaEdge, Props as NextJSLambdaEdgeProps } from "@sls-next/cdk-construct";
import type { StackProps } from "aws-cdk-lib";
import { builder } from "../bin/cdk";

export class CdkStack extends Stack {
	constructor(scope: Construct, id: string, props?: StackProps) {
		super(scope, id, props);

		const domainName = process.env.DOMAIN_NAME;
		const hostedZoneId = process.env.ZONE_ID;
		const zoneName = process.env.ZONE_NAME;

		let domain: NextJSLambdaEdgeProps["domain"];

		if (domainName && hostedZoneId && zoneName) {
			const hostedZone = HostedZone.fromHostedZoneAttributes(this, "hostedZone", { hostedZoneId, zoneName });
			const certificate = new Certificate(this, "certificate", {
				domainName,
				validation: CertificateValidation.fromDns(hostedZone)
			});

			domain = { domainNames: [domainName], certificate, hostedZone };
		}

		const { distribution } = new NextJSLambdaEdge(this, "NextJSLambdaEdge", {
			serverlessBuildOutDir: builder.outputDir,
			domain
		});

		new CfnOutput(this, "appUrl", { value: `https://${domainName || distribution.domainName}` });
	}
}
